@extends('layouts.main')

@section('jumbotron')
    <h1 class="display-3">{{$user->firstName}}</h1>
    <p>{{$user->lastName}}</p>
@endsection

@section('main_content')
    <div class="col-md-12">
        <form action="{{route('account.destroy', ["user" => $user->id])}}" method="post">
            {{method_field('delete')}}
            {{csrf_field()}}
            <h2>Are you sure you want to delete <b>{{$user->firstName}}</b> ?</h2>
            <div class="form-group">
                <button class="btn btn-danger">yes</button>
                <a class="btn btn-warning" href="{{route('account.index')}}">no</a>
            </div>
        </form>
    </div>
@endsection