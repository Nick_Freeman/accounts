@extends('layouts/main')

@section('main_content')



@endsection

@section('jumbotron')


<input type="hidden" id="urlEdit" value="{{route('account.edit')}}">
<input type="hidden" id="urlDestroy" value="{{route('account.delete')}}">

    <div class="account_table">
        <table class="table table-bordered table-dark" id="print">
            <tr>
                <th>Имя</th>
                <th>Фамилия</th>
                <th>Номер телефона</th>
                <th>Email</th>
                @if(Auth::user()->role == 'admin')
                    <th></th>
                @endif
            </tr>
            <div >
                    <input type="hidden" id="csrf" value="{{csrf_token()}}" />
                    <input  name="url" id="url" type="hidden"  value="{{route('account.ajax.table')}}">
            </div>



        </table>

    </div>





@endsection