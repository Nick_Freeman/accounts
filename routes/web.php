<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});


/** Registration ROUTES*/
Route::group(['prefix' => 'registration'], function () {

    Route::get('/create', 'RegistrationController@create')->name('registration.create');
    Route::put('/store', 'RegistrationController@store')->name('registration.store');

});

/**Login ROUTES*/
Route::group(['prefix' => 'login'], function () {

    Route::get('/destroy', 'LoginsController@destroy')->name('login.destroy');
    Route::get('/create','LoginsController@create')->name('login.create');
    Route::post('/store','LoginsController@store')->name('login.store');
});

/**Word ROUTES*/

Route::group(['prefix' => 'account'], function () {
    Route::get('/', 'AccountsController@index')->name('account.index');
    Route::post('ajaxTable','AccountsController@tableToAjax')->name('account.ajax.table');
});
Route::group(['prefix' => 'account', 'middleware' => ['role']], function () {
    Route::get('/create', 'AccountsController@create')->name('account.create');
    Route::put('/store', 'AccountsController@store')->name('account.store');
    Route::get('/edit/{user?}', 'AccountsController@edit') ->name('account.edit');
    Route::put('/update/{user?}', 'AccountsController@update')->name('account.update');
    Route::get('/delete/{user?}', 'AccountsController@delete')->name('account.delete');
    Route::delete('/destroy/{user?}', 'AccountsController@destroy')->name('account.destroy');
});