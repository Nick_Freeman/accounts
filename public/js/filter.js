
$( document ).ready(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('#csrf').val()
        }
    });

    $.ajax({
        url: $('#url').val() ,
        method: "POST",

    }).done(function(mass) {
        // console.log(mass);
        let output = '';
        let inputEdit = document.getElementById("urlEdit").value;
        let inputDestroy = document.getElementById("urlDestroy").value;

            for (let i = 0; i < mass.isResult.length; i++) {
                    output += '<tr>' +
                        '<td>' + mass.isResult[i].firstName + '</td>'+
                        '<td>' + mass.isResult[i].lastName + '</td>' +
                        '<td>' + mass.isResult[i].phone + '</td>' +
                        '<td>' + mass.isResult[i].email + '</td>';

                if(mass.isAdmin == true) {
                output += '<td><a class="btn btn-warning" href="'+inputEdit+'/'+mass.isResult[i].id+'" role="button"><span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>'+
                          '<a class="btn btn-danger" href="'+inputDestroy+'/'+mass.isResult[i].id+'" role="button"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>';
                }

                output +='</tr>';



        }


        $('#print').append(output)

    });

});

