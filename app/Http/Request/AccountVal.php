<?php


namespace App\Http\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class AccountVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

//            dd(Request::route()->getName());

        switch (Request::route()->getName()) {
            case 'account.store':
                return [
                    'firstName' => 'required|max:8|min:3',
                    'lastName' => 'required|max:8|min:3',
                    'phone' => 'required',
                    'role' => 'required',
                    'email' => 'required|email',
                    'password' => 'required|max:15|min:6'
                ];
                break;
            case 'registration.store':
                return [
                    'firstName' => 'required|max:8|min:3',
                    'lastName' => 'required|max:8|min:3',
                    'phone' => 'required',
                    'role' => 'required',
                    'email' => 'required|email',
                    'password' => 'required|max:15|min:6'
                ];
                break;
            case'account.update':
                return [
                    'firstName' => 'required|max:8|min:3',
                    'lastName' => 'required|max:8|min:3',
                    'phone' => 'required',
                    'role' => 'required',
                    'email' => 'required|email',
                    'password' => 'required|max:15|min:6'
                ];
                break;
        };

    }
    //php artisan make:request StoreBlogPost

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */

    public function messages()
    {
        return [
            'firstName.max' => 'Field is required,and max length 8 symbol',
            'firstName.min' => 'Field is required,and min length 3 symbol',
            'lastName.max' => 'Field is required,and max length 8 symbol',
            'lastName.min' => 'Field is required,and min length 3 symbol',
            'phone' =>'Field is required',
            'role' => 'Field is required',
            'email' => 'Field is required,',
            'password.max' => 'Field is required,and max length 15 symbol',
            'password.min' => 'Field is required,and min length 6 symbol',
        ];
    }
}