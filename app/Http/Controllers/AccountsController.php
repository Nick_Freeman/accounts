<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Request\AccountVal;

class AccountsController extends Controller
{
    public function template($account ,$request)
    {

        $account->fill($request->only('firstName','lastName','phone','email'));
        $account->save();

        return json_encode([
            'firstName' => $account->firstName,
            'lastName' => $account->lastName,
            'phone' => $account->phone,
            'email' => $account->email

        ]);
    }


    public function index(){
        $id = Auth::id();
        $account = User::find($id);

        return view('accounts.index')->with('account',$account) ;
    }

   public function edit(User $user){
       return view('accounts.edit', compact('user'));
   }

    public function tableToAjax()
    {
        return response(['isResult'=>User::all(),'isAdmin'=>Auth::user()->role == 'admin']);
    }

    public function create(){
        return view('accounts.create');
    }

    public function delete(User $user){
        return view('accounts.delete', compact('user'));
    }

    public function update (Request $request,$user){
        $user= User::find($request->user);
        if ($user) {
            $this->template($user,$request);

            return redirect()->route('account.index') ;
        }

        return redirect()->back();
    }

    public function store (AccountVal $request){
        $account = new User();
        $this->template($account,$request);

        return redirect(route('account.index'));
    }

    public function destroy(User $user){

        $user -> delete();

        return redirect()->route('account.index') ;
    }
}