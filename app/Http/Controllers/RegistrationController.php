<?php

namespace App\Http\Controllers;

use App\Http\Request\AccountVal;
use App\User;


class RegistrationController extends Controller
{
    public function __construct()
    {
        $this->model = 'App\User';
        $this->rootView = 'users';
        $this->outPutVariable = 'users';
        $this->outPutVariableForSingle = 'user';
        $this->baseRouteName = 'user';
    }


    public function create(){
        return view('users.create');
    }

    public function store(AccountVal $request){


        $user = new User();

        $user->fill($request->only('firstName','lastName','phone','role','email','password'));

        $user ->password = bcrypt($user["password"]);
        $user->save();

        auth()->login($user);


        return redirect('/');
    }
}